import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:url_launcher/url_launcher.dart';
import 'dart:async';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:share/share.dart';

final ThemeData kIOSTheme = new ThemeData(
  primarySwatch: Colors.red,
  primaryColor: Colors.grey[100],
  primaryColorBrightness: Brightness.light,
);

final ThemeData kDefaultTheme = new ThemeData(
  primarySwatch: Colors.red,
  accentColor: Colors.redAccent[400],
);

final reference = FirebaseDatabase.instance.reference().child('movies');
final googleSignIn = new GoogleSignIn();
final auth = FirebaseAuth.instance;
final TextStyle categoryStyle = new TextStyle(color: Colors.redAccent);
bool visibilityTag = false;

void main() {
  runApp(new FlutterCinemaApp());
}

class FlutterCinemaApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: "Flutter Cinema",
      theme: defaultTargetPlatform == TargetPlatform.iOS
          ? kIOSTheme
          : kDefaultTheme,
      home: new HomeScreen(),
    );
  }
}

class HomeScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new HomeScreenState();
  }
}

class HomeScreenState extends State<HomeScreen> {
  final key = new GlobalKey<ScaffoldState>();
  GoogleSignInAccount user = googleSignIn.currentUser;

  Future<Null> _ensureLoggedIn() async {
    if (user == null) user = await googleSignIn.signInSilently();
    if (user == null) {
      await googleSignIn.signIn();
    }

    _changed(true);

    if (await auth.currentUser() == null) {
      GoogleSignInAuthentication credentials =
          await googleSignIn.currentUser.authentication;
      await auth.signInWithGoogle(
        idToken: credentials.idToken,
        accessToken: credentials.accessToken,
      );
      _changed(true);
    } else {
      key.currentState.showSnackBar(
        new SnackBar(
          content: new Text("Are you sure you want to sign out?"),
          action: new SnackBarAction(
            label: "Sign out",
            onPressed: _signOut,
          ),
        ),
      );
    }
  }

  void _signOut() {
    _changed(false);
    auth.signOut();
    key.currentState.hideCurrentSnackBar();
  }

  void _changed(bool visibility) {
    setState(() {
      visibilityTag = visibility;
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: key,
      appBar: new AppBar(
        title: new Text("Flutter Cinema"),
        elevation: Theme.of(context).platform == TargetPlatform.iOS ? 0.0 : 4.0,
        centerTitle: true,
        actions: <Widget>[
          new IconButton(
              icon: new Icon(
                Icons.account_circle,
                color: Theme.of(context).platform == TargetPlatform.iOS
                    ? Colors.red
                    : Colors.white,
              ),
              onPressed: _ensureLoggedIn)
        ],
      ),
      body: new Container(
        child: new Column(
          children: <Widget>[
            new Flexible(
              child: new FirebaseAnimatedList(
                query: reference,
                scrollDirection: Axis.horizontal,
                padding: new EdgeInsets.all(8.0),
                reverse: false,
                itemBuilder:
                    (_, DataSnapshot snapshot, Animation<double> animation) {
                  return new MovieItem(snapshot: snapshot);
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class MovieItem extends StatelessWidget {
  MovieItem({this.snapshot});

  final DataSnapshot snapshot;

  @override
  Widget build(BuildContext context) {
    return new GestureDetector(
        onTap: () {
          Movie movie = new Movie(
              snapshot.value['name'],
              snapshot.value['release'],
              snapshot.value['country'],
              snapshot.value['description'],
              snapshot.value['distribution'],
              snapshot.value['genre'],
              snapshot.value['length'],
              snapshot.value['photoURL'],
              snapshot.value['cast'],
              snapshot.value['videoURL']);

          Navigator.push(
              context,
              new MaterialPageRoute(
                  builder: (_) => new MovieDetailScreen(movie)));
        },
        child: new Container(
          margin: const EdgeInsets.all(2.0),
          child: new Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              new Container(
                child: new Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    new Image(
                      image: new NetworkImage(snapshot.value['photoURL']),
                      width: 250.0,
                      height: 300.0,
                    ),
                    new Padding(
                        padding: const EdgeInsets.symmetric(vertical: 8.0),
                        child: new Text(
                          snapshot.value['name'],
                          style: Theme.of(context).textTheme.headline,
                        )),
                    new Text(
                      snapshot.value['release'],
                      style: Theme.of(context).textTheme.subhead,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ));
  }
}

class Movie {
  String name;
  String release;
  String country;
  String description;
  String distribution;
  String genre;
  int length;
  String photoURL;
  String cast;
  String videoURL;

  Movie(
      this.name,
      this.release,
      this.country,
      this.description,
      this.distribution,
      this.genre,
      this.length,
      this.photoURL,
      this.cast,
      this.videoURL);
}

class MovieDetailScreen extends StatefulWidget {
  final Movie movie;

  MovieDetailScreen(this.movie, {Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return new MovieDetailState();
  }
}

class MovieDetailState extends State<MovieDetailScreen> {
  Future<Null> _launched;
  String _videoUrl;
  String movieName;

  void _launchUrl() {
    setState(() {
      _launched = _launch(_videoUrl);
    });
  }

  Future<Null> _launch(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  Widget _launchStatus(BuildContext context, AsyncSnapshot<Null> snapshot) {
    if (snapshot.hasError) {
      return new Text('Error: ${snapshot.error}');
    } else {
      return const Text('');
    }
  }

  @override
  Widget build(BuildContext context) {
    _videoUrl = widget.movie.videoURL;
    movieName = widget.movie.name;
    return new Scaffold(
        appBar: new AppBar(
            title: new Text(widget.movie.name),
            elevation:
                Theme.of(context).platform == TargetPlatform.iOS ? 0.0 : 4.0,
            centerTitle: true,
            actions: <Widget>[
              new IconButton(
                icon: new Icon(
                  Icons.share,
                  color: Theme.of(context).platform == TargetPlatform.iOS
                      ? Colors.red
                      : Colors.white,
                ),
                onPressed: movieName.isNotEmpty
                    ? () {
                        share('Wow! Movie ' +
                            movieName +
                            ' is going to the Flutter cinema!');
                      }
                    : null,
                tooltip: 'Share movie',
              ),
            ]),
        body: new ListView(
          shrinkWrap: true,
          padding: const EdgeInsets.all(20.0),
          children: <Widget>[
            new Image(
              image: new NetworkImage(widget.movie.photoURL),
              width: 250.0,
              height: 300.0,
            ),
            new Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: new Text(
                  widget.movie.name,
                  style: Theme.of(context).textTheme.headline,
                  textAlign: TextAlign.center,
                )),
            new Column(
              children: <Widget>[
                new Text(
                  "Release:",
                  style: categoryStyle,
                ),
                new Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 8.0, horizontal: 16.0),
                    child: new Text(
                      widget.movie.release,
                      style: Theme.of(context).textTheme.subhead,
                    )),
              ],
            ),
            new Column(
              children: <Widget>[
                new Text(
                  "Distribution:",
                  style: categoryStyle,
                ),
                new Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 8.0, horizontal: 16.0),
                    child: new Text(
                      widget.movie.distribution,
                      style: Theme.of(context).textTheme.subhead,
                    )),
              ],
            ),
            new Column(
              children: <Widget>[
                new Text(
                  "Length:",
                  style: categoryStyle,
                ),
                new Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 8.0, horizontal: 16.0),
                    child: new Text(
                      widget.movie.length.toString() + " minutes",
                      style: Theme.of(context).textTheme.subhead,
                    )),
              ],
            ),
            new Column(
              children: <Widget>[
                new Text(
                  "Country:",
                  style: categoryStyle,
                ),
                new Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 8.0, horizontal: 16.0),
                    child: new Text(
                      widget.movie.country,
                      style: Theme.of(context).textTheme.subhead,
                    )),
              ],
            ),
            new Column(
              children: <Widget>[
                new Text(
                  "Genre:",
                  style: categoryStyle,
                ),
                new Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 8.0, horizontal: 16.0),
                    child: new Text(
                      widget.movie.genre,
                      style: Theme.of(context).textTheme.subhead,
                    )),
              ],
            ),
            new Column(
              children: <Widget>[
                new Text(
                  "Cast:",
                  style: categoryStyle,
                ),
                new Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 8.0, horizontal: 16.0),
                    child: new Text(
                      widget.movie.cast,
                      style: Theme.of(context).textTheme.subhead,
                    )),
              ],
            ),
            new Column(
              children: <Widget>[
                new Text(
                  "Description:",
                  style: categoryStyle,
                ),
                new Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 8.0, horizontal: 16.0),
                    child: new Text(
                      widget.movie.description,
                      style: Theme.of(context).textTheme.subhead,
                    )),
              ],
            ),
            new Column(
              children: <Widget>[
                new RaisedButton(
                    onPressed: _launchUrl,
                    child: new Container(
                        margin: const EdgeInsets.symmetric(vertical: 16.0),
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            new Icon(Icons.movie, color: Colors.redAccent),
                            new Padding(
                                padding: const EdgeInsets.symmetric(
                                    vertical: 8.0, horizontal: 16.0),
                                child: new Text(
                                  "Watch trailer",
                                  style: categoryStyle,
                                )),
                          ],
                        ))),
                visibilityTag
                    ? new Container(
                        margin: const EdgeInsets.only(top: 16.0),
                        child: new RaisedButton(
                            onPressed: null,
                            child: new Container(
                                margin:
                                    const EdgeInsets.symmetric(vertical: 16.0),
                                child: new Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    new Icon(Icons.local_play,
                                        color: Colors.redAccent),
                                    new Padding(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 8.0, horizontal: 16.0),
                                        child: new Text(
                                          "Buy tickets",
                                          style: categoryStyle,
                                        )),
                                  ],
                                ))))
                    : new Container(),
              ],
            ),
            new FutureBuilder<Null>(future: _launched, builder: _launchStatus),
          ],
        ));
  }
}
